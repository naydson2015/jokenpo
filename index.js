import { AppRegistry, View, Text, Button, Image, StyleSheet } from 'react-native';
import React, { Component } from 'react';
import Topo from './src/components/topo';
import Icone from './src/components/icone';

class app3 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      escolhaUsuario: '',
      escolhaComputador: '',
      resultado: ''
    }
  }

  jokenpo(escolhaUsuario) {

    //gerar numero aleatorio escolha PC (0,1,2: pedra/papel/tesoura)
    const numAleatorio = Math.floor(Math.random() * 3);
    let escolhaComputador = '';

    switch (numAleatorio) {
      case 0: escolhaComputador = 'Pedra'; break;
      case 1: escolhaComputador = 'Papel'; break;
      case 2: escolhaComputador = 'Tesoura'; break;
      default: escolhaComputador = 'Erro';
    }

    let resultado = '';
    if (escolhaComputador === 'Pedra') {
      if (escolhaUsuario === 'Pedra') {
        resultado = 'Empate...';
      } else if (escolhaUsuario === 'Papel') {
        resultado = 'Você Ganhou !!';
      } else {
        resultado = 'Você perdeu.';
      }
    }

    if (escolhaComputador === 'Papel') {
      if (escolhaUsuario === 'Pedra') {
        resultado = 'Você perdeu.';
      } else if (escolhaUsuario === 'Papel') {
        resultado = 'Empate...';
      } else {
        resultado = 'Você ganhou!!';
      }
    }

    if (escolhaComputador === 'Tesoura') {
      if (escolhaUsuario === 'Pedra') {
        resultado = 'Você ganhou!!';
      } else if (escolhaUsuario === 'Papel') {
        resultado = 'Você perdeu.';
      } else {
        resultado = 'Empate...';
      }
    }

    this.setState({ //sem necessidade de passar chave pois os nomes variáveis e chaves são iguais
      escolhaUsuario,
      escolhaComputador,
      resultado
    });
  }

  render() {
    return (
      <View>
        <Topo />

        <View style={styles.painelAcoes}>
          <View style={styles.btnEscolha}>
            <Button title="Pedra" onPress={() => { this.jokenpo("Pedra") }} />
          </View>
          <View style={styles.btnEscolha}>
            <Button title="Papel" onPress={() => { this.jokenpo("Papel") }} />
          </View>
          <View style={styles.btnEscolha}>
            <Button title="Tesoura" onPress={() => { this.jokenpo("Tesoura") }} />
          </View>
        </View>

        <View style={styles.palco}>
          <Text style={styles.txtResultado}> {this.state.resultado} </Text>

          <Icone escolha={this.state.escolhaComputador} jogador='Computador' />
          <Icone escolha={this.state.escolhaUsuario} jogador='Você' />

        </View>

      </View >
    );
  }
}

const styles = StyleSheet.create({
  btnEscolha: {
    width: 90
  },

  painelAcoes: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: 12
  },

  palco: {
    alignItems: 'center',
    marginTop: 16
  },

  txtResultado: {
    fontSize: 25,
    fontWeight: "bold",
    color: 'red'
  },
});

AppRegistry.registerComponent('jokenpo', () => app3);