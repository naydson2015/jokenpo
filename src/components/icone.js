import { View, StyleSheet, Text, Image } from 'react-native';
import React, { Component } from 'react';

const pedra = require('../../imgs/pedra.png');
const papel = require('../../imgs/papel.png');
const tesoura = require('../../imgs/tesoura.png');

class Icone extends Component {
  render() {
    if (this.props.escolha === 'Pedra') {
      return (
        <View style={styles.icone}>
          <Text style={styles.txtJogador}>{this.props.jogador}</Text>
          <Image source={pedra} />
        </View>
      );
    } else if (this.props.escolha === 'Papel') {
      return (
        <View style={styles.icone}>
          <Text style={styles.txtJogador}>{this.props.jogador}</Text>
          <Image source={papel} />
        </View>
      );
    } else if (this.props.escolha === 'Tesoura') {
      return (
        <View style={styles.icone}>
          <Text style={styles.txtJogador}>{this.props.jogador}</Text>
          <Image source={tesoura} />
        </View>
      );
    }
    return false;
  }
}

const styles = StyleSheet.create({
  txtJogador: {
    fontSize: 18
  },

  icone: {
    alignItems: 'center',
    marginBottom: 16,
    marginTop: 16
  }
});

export default Icone;
