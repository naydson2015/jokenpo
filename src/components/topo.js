import { View, Image, StyleSheet } from 'react-native';
import React, { Component } from 'react';

const imagem = require('../../imgs/original.png');

class Topo extends Component {
  render() {
    return (
      <View style={styles.viewImage}>
        <Image style={styles.imagemTopo} source={imagem} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewImage: {
    alignItems: 'center',
  },

  imagemTopo: {
    width: '100%',
  }
})

export default Topo;
